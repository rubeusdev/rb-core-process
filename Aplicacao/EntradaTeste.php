<?php

namespace Rubeus\Processo\Aplicacao;

use Rubeus\FrontController\Saida as Saida;
use Rubeus\Processo\Dominio\Processo\RepositorioProcesso as RepositorioProcesso;
use Rubeus\Bd\Persistencia as Persistencia;
use Rubeus\ManipulacaoEntidade\Dominio\ConteinerEntidade;

class EntradaTeste
{

    private function configurar()
    {
        //ini_set('display_startup_errors',1);

        set_time_limit(9999999999);
        ini_set('memory_limit', '-1');

        ini_set('display_errors', 1);
        error_reporting(-1);

        //error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
        //RepositorioRegra::setTeste(1);
        Persistencia::setGuardar(1);
    }

    public function iniciar($codProcesso, $codEtapa)
    {

        $resultado = array();
        $this->configurar();
        //        include  '/var/www/html/multivixmy/src/Integracao/Aplicacao/IntegracaoBase/IniciarBaseLocal.php';
        //        var_dump(file_exists('/var/www/html/multivixmy/src/Integracao/Aplicacao/IntegracaoBase/IniciarBaseLocal.php'));
        //        $teste = new \Integracao\Aplicacao\IntegracaoBase\IniciarBaseLocal();

        $usuarioRubeusDebugandoEmProducao = false;
        $entidade = ConteinerEntidade::getInstancia('Usuario');

        if ($entidade && method_exists($entidade, 'getEmail')) {
            $entidade->setId($_SESSION['dadosUsuarioLogado']['id']);
            $entidade->carregar();
            $usuarioRubeusDebugandoEmProducao = str_ends_with($entidade->getEmail(), '@rubeus.com.br');
        }

        $processo = RepositorioProcesso::get($codProcesso, $codEtapa);

        $resultado['resultado'] = $processo->executar();
        if ((defined('APLICACAO_PRODUCAO') && APLICACAO_PRODUCAO <> 1) ||
            (defined('DEBUG_ATIVO') && DEBUG_ATIVO == 1) || $usuarioRubeusDebugandoEmProducao
        ) {
            $resultado['recebido'] = array($_POST, $_GET);
            $resultado['sentencas'] = Persistencia::getSentencas();
        }

        Saida::ecoar($resultado);
    }
}
