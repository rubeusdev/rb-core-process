<?php

namespace Rubeus\Processo\Aplicacao;

use Rubeus\FrontController\Saida;
use Rubeus\Processo\Dominio\Processo\RepositorioProcesso;
use Rubeus\Processo\Dominio\Exception\ExceptionProcesso;
use Rubeus\Processo\Dominio\Exception\ExceptionEvento;
use Rubeus\Processo\Dominio\Exception\ExceptionRegra;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\ManipulacaoEntidade\Dominio\ConteinerEntidade;
use Rubeus\Servicos\Entrada\I as I;
//ini_set('display_startup_errors',1);
//ini_set('display_errors',1);
//error_reporting(-1);

class Entrada
{

    public function iniciar($codProcesso, $codEtapa)
    {
        if (defined('MONITORAR_DESEMPENHO')) {
            // if (MONITORAR_DESEMPENHO == 1) {
            $id = \Rubeus\Bd\Persistencia::execultar("insert into monitoramentodesempenho(metodo, url, dados, inicio) values ('" . $codProcesso . '/' . $codEtapa . "', '" . I::server('REQUEST_URI') . "', '" . json_encode(['POST' => $_POST, 'GET' => $_GET, 'JSON' => I::getDataJson(), 'INPUT' => file_get_contents("php://input"), 'REQUEST' => $_REQUEST, 'SERVER' => $_SERVER]) . "', now(6));");
            if (MONITORAR_DESEMPENHO == 1) {
                \Rubeus\Bd\Persistencia::setGuardar(1);
            }
            \Rubeus\Bd\Persistencia::commit();
            // } else {
            $date = date('Y-m-d H:i:s');
            // }
        }

        if ((defined('APLICACAO_PRODUCAO') && APLICACAO_PRODUCAO <> 1) ||
            (defined('DEBUG_ATIVO') && DEBUG_ATIVO == 1) ||
            (defined('DEBUG_PRODUCAO') && DEBUG_PRODUCAO == 1)
        ) {
            \Rubeus\Bd\Persistencia::setGuardar(1);
        }
        try {
            $processo = RepositorioProcesso::get($codProcesso, $codEtapa);
            $resultado = $processo->executar(false, true);
            $entidade = Conteiner::get('LogProcessoGeral');
            if ($entidade) {
                $entidade->registrar($resultado);
            }
        } catch (ExceptionProcesso $e) {
            echo $e->getMessagem();
        } catch (ExceptionEvento $e) {
            echo $e->getMessagem();
        } catch (ExceptionRegra $e) {
            echo $e->getMessagem();
        }

        if (defined('MONITORAR_DESEMPENHO')) {
            if (MONITORAR_DESEMPENHO == 1) {
                $query = \Rubeus\Bd\Persistencia::getSentencas();
                \Rubeus\Bd\Persistencia::consultar(false, "update monitoramentodesempenho set fim = now(6), qtdquery = " . count($query) . " where id = " . $id . ";");
                for ($i = 0; $i < count($query); $i++) {
                    // self::$sentencas[] = array('base'=>self::$objConecta->getDb(),'sentencas'=>$sql,'parametros' => $parametro, 'start' => $start,'end' => $end);
                    if (strpos($query[$i]['sentencas'], 'monitoramentodesempenho') === false && strpos($query[$i]['sentencas'], 'idabancodedados') === false) {
                        \Rubeus\Bd\Persistencia::execultar("insert into idabancodedados(monitoramentodesempenho_id, `sql`, parametros, inicio, termino, momento) values ('" . $id . "', '" . str_replace("'", "\'", $query[$i]['sentencas']) . "', '" . (is_null($query[$i]['parametros']) ? '' : json_encode($query[$i]['parametros'])) . "', " . (is_null($query[$i]['start']) ? 'null' : "'" . json_encode($query[$i]['start']) . "'") . "," . (is_null($query[$i]['end']) ? 'null' : "'" . json_encode($query[$i]['end']) . "'") . ", now(6));");
                    }
                }
            } else {
                $date2 = date('Y-m-d H:i:s');
                if (strtotime($date2) - strtotime($date) > 10) {
                    \Rubeus\Bd\Persistencia::execultar("insert into monitoramentodesempenho_v2(metodo, url, dados, inicio, fim) values ('" . $codProcesso . '/' . $codEtapa . "', '" . I::server('REQUEST_URI') . "', '" . json_encode(['POST' => $_POST, 'GET' => $_GET, 'JSON' => I::getDataJson(), 'INPUT' => file_get_contents("php://input"), 'REQUEST' => $_REQUEST, 'SERVER' => $_SERVER]) . "','" . $date . "','" . $date2 . "');");
                }
                if ($id) {
                    \Rubeus\Bd\Persistencia::consultar(false, "delete from monitoramentodesempenho where id = " . $id . ";");
                }
            }
            \Rubeus\Bd\Persistencia::commit();
        }

        $usuarioRubeusDebugandoEmProducao = false;
        if (defined('DEBUG_PRODUCAO') && DEBUG_PRODUCAO == 1) {
            $entidade = ConteinerEntidade::getInstancia('Usuario');
            if ($entidade) {
                $entidade->setId($_SESSION['dadosUsuarioLogado']['id']);
                $entidade->carregar();
                $usuarioRubeusDebugandoEmProducao = str_ends_with($entidade->getEmail(), '@rubeus.com.br');
            }
        }

        if ((defined('APLICACAO_PRODUCAO') && APLICACAO_PRODUCAO <> 1) ||
            (defined('DEBUG_ATIVO') && DEBUG_ATIVO == 1) || $usuarioRubeusDebugandoEmProducao
        ) {
            $log = Conteiner::get('Log');
            if ($log) {
                $log->gravar(json_encode(['POST' => $_POST, 'GET' => $_GET, 'JSON' => I::getDataJson(), 'INPUT' => file_get_contents("php://input")]), json_encode($resultado), $codProcesso . '/' . $codEtapa, \Rubeus\Bd\Persistencia::getSentencas(), 0);
            }
            $resultado['recebido'] = array(['POST' => $_POST, 'GET' => $_GET, 'JSON' => I::getDataJson(), 'INPUT' => file_get_contents("php://input")]);
            $resultado['sentenca'] = \Rubeus\Bd\Persistencia::getSentencas();
            $resultado['atividades'] = \Rubeus\Processo\Dominio\Atividade\Atividade::getHistorico();
        }
        Saida::ecoar($resultado);
    }
}
