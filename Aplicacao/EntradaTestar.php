<?php
namespace Rubeus\Processo\Aplicacao;
use Rubeus\Processo\Dominio\Processo\RepositorioProcesso as RepositorioProcesso;
use Rubeus\Bd\Persistencia as Persistencia;

class EntradaTestar{ 
    
    private function configurar(){        
        Persistencia::setGuardar(1);
    } 
    
    public function iniciar($codProcesso, $codEtapa){
        
        $resultado = array();
        
        $this->configurar();
        
        $processo = RepositorioProcesso::get($codProcesso, $codEtapa);
        
        $resultado['resultado'] = $processo->executar(); 
        
        $resultado['sentencas'] = Persistencia::getSentencas();
        
        return $resultado;
        //Saida::ecoar($resultado);
    }
      
}

