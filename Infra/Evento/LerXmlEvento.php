<?php
namespace Rubeus\Processo\Infra\Evento;
use  Rubeus\Servicos\XML\XML as XML;

abstract class LerXmlEvento{
    private static $xml;
    
    public  static function lerXmlEvento(){
        self::$xml = XML::ler(__DIR__.'/../dados/eventos/evento.xml');
        if(self::$xml){
            self::lerEvento();
            self::$xml = null;
            return true;
        }
        return false;
    }
    
    private static function lerEvento(){
        foreach(self::$xml->evento as $evento){
            $acao = self::lerAcao($evento);
            $processo = self::lerProcesso($evento);

            RepositorioEvento::add(trim($evento['codigo']), 
                    array('acao' => $acao, 'processo' => $processo));  
        }
    }
    
    private static function lerAcao($evento){
        $acoes = array();
        foreach($evento->acao as $acao)
            $acoes[] = array('entidade' => trim($acao['entidade']),'metodo' => trim($acao['metodo']) );
        return $acoes;
    }
    
    private static function lerProcesso($evento){
        $processos = array();
        foreach($evento->processo as $processo)
            $processos[] = array('codigo' => trim($processo['codigo']),'etapa' => trim($processo['etapa']));
       
        return $processos;
    }
    
}