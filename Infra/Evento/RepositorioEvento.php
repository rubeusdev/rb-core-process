<?php
namespace Rubeus\Processo\Infra\Evento;
use Rubeus\Servicos\Vetor\VetorAssociativo;

abstract class RepositorioEvento{
    private static $evento;
    
    public static function consultarEvento($evento){
        if(!isset(self::$evento[$evento]))
            LerXmlEvento::lerXmlEvento($evento);
        return self::$evento[$evento];
    }
    
    public static function add($chave, $dados){
        VetorAssociativo::set(self::$evento, $dados, $chave);
    }

    public  static function liberar($chave){
        VetorAssociativo::limpar(self::$evento, $chave);
    }
}