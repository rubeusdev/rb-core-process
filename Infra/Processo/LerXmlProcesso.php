<?php
namespace Rubeus\Processo\Infra\Processo;
use  Rubeus\Servicos\XML\XML as XML;

abstract class LerXmlProcesso{
    private static $xml;
    private static $etapa;
    private static $pasta;

    public static function setPasta($pasta){
        self::$pasta = $pasta;
    }

    public  static function lerXmlProcesso($processo, $etapa=false){
        $dir = new \DirectoryIterator(self::$pasta);
        foreach ($dir as $file){
            if ($file->isDot()) continue;
            if($etapa) self::$xml = XML::ler(self::$pasta.'/'.$file->getFilename().'/'.$processo.'/'.$etapa.'.xml');
            else self::$xml = XML::ler(self::$pasta.'/'.$file->getFilename().'/'.$processo.'/'.$processo.'.xml');

            if (self::$xml){
//                echo __DIR__.'/../dados/processos/'.$file->getFilename().'/'.$processo.'/'.$etapa.'.xml';
                break;
            }
        }
        if(!self::$xml){
            if($etapa) self::$xml = XML::ler(self::$pasta.'/'.$processo.'/'.$etapa.'.xml');
            else self::$xml = XML::ler(self::$pasta.'/'.$processo.'/'.$processo.'.xml');
        }

        self::$etapa = $processo;

        if(self::$xml){
            self::lerProcesso();
            self::$xml = null;
            return true;
        }
        return false;
    }

    private static function lerProcesso(){
        RepositorioProcesso::add(self::$etapa .'/'.trim(self::$xml['codigo']), array('eventoInicial' => array(), 'campos' => array(),
                                                                    'atividades'=>array(), 'condicional' => array(), 'eventoFinal' => array(),
                                                                    'entidadeRetorno' => trim(self::$xml['entidadeRetorno']),
                                                                    'etapaRetorno' => trim(self::$xml['etapaRetorno']),
                                                                    'permissao' => trim(self::$xml['permissao']),
                                                                    'naoUtilizarTransacao' => trim(self::$xml['naoUtilizarTransacao']),
                                                                    'metodo' => trim(self::$xml['metodo']),
                                                                    'campoPermisao' => trim(self::$xml['campoPermissao'])));
        self::lerEventoInicial();
        self::lerEventoFinal();
        self::lerCampos();
        self::lerAtividades();
    }

    private static function lerEventoInicial(){
        $eventos = array();
        if(is_array(self::$xml->entrada->evento)){
            foreach(self::$xml->entrada->evento as $evento){
                $eventos[] =  trim($evento['codigo']);
            }
        }else $eventos[] = trim(self::$xml->entrada->evento['codigo']);

        RepositorioProcesso::add(self::$etapa .'/'.trim(self::$xml['codigo']).',eventoInicial', $eventos);
    }

    private static function lerEventoFinal(){
        $eventos = array();
        foreach(self::$xml->saida->evento as $evento){
            $eventos[] =  trim($evento['codigo']);
        }

        RepositorioProcesso::add(self::$etapa .'/'.trim(self::$xml['codigo']).',eventoFinal', $eventos);
    }

    private static function lerCampos(){
        if(isset(self::$xml->entrada->campo)){
            $campos = array();
            foreach(self::$xml->entrada->campo as $campo){
                $campos[trim($campo['chave'])] =   array(   'regra' => trim($campo['regra']),
                                                            'chave' => trim($campo['chave']),
                                                            'json' => trim($campo['json']),
                                                            'msg' => self::mensagemCampo($campo),
                                                            'requisicao' => trim($campo['requisicao']),
                                                            'valorPadrao' => isset($campo['valorPadrao']) ? trim($campo['valorPadrao']):"",
                                                            'nome' => trim($campo['nome']));
            }
            RepositorioProcesso::add(self::$etapa .'/'.trim(self::$xml['codigo']).',campos', $campos);
        }
    }

    private static function mensagemCampo($campo){
        $mensagem = array();
        foreach($campo->msg as $chave => $valor)
            $mensagem[trim($chave)] =  trim($valor['correspondente']);
        return $mensagem;
    }

    private static function lerAtividades(){
        $atividade = array();
        foreach(self::$xml->atividades->atividade as $atividade){
            $atividades[trim($atividade['codigo'])] =   array(  'tipo' => trim($atividade['tipo']),
                                                                'proximo' => trim($atividade['proximo']),
                                                                'commit' => trim($atividade['commit']),
                                                                'codigo' => trim($atividade['codigo']),
                                                                'metodo' => trim($atividade->entidade['metodo']),
                                                                'entidade'=> trim($atividade->entidade['nome']));
        }
        RepositorioProcesso::add(self::$etapa .'/'.trim(self::$xml['codigo']).',atividades', $atividades);
    }

}
