<?php
namespace Rubeus\Processo\Infra\Processo;
use Rubeus\Servicos\Vetor\VetorAssociativo;
use Rubeus\Servicos\Json\Json;
use Rubeus\ContenerDependencia\Conteiner;

abstract class RepositorioProcesso{
    private static $processos;
    private static $pasta = false;

    public static function consultarProcesso($processo, $etapa){
        self::pasta();
        $inciceValido = $etapa ? $processo.'/'.$etapa : $processo;
        if(!isset(self::$processos[$inciceValido])){
            for($i=0; $i<count(self::$pasta);$i++){
                LerXmlProcesso::setPasta(self::$pasta[$i]);
                if(LerXmlProcesso::lerXmlProcesso($processo, $etapa)){
                    break;
                }
            }
        }
        return self::$processos[$inciceValido];
        
    }
    
    public static function add($chave, $dados){
        VetorAssociativo::set(self::$processos, $dados, $chave);
    }

    public  static function liberar($chave){
        VetorAssociativo::limpar(self::$processos, $chave);
    }

    public  static function pasta(){
        if(!self::$pasta){
            self::$pasta = Conteiner::get('diretorioProcesso');
        }
    }
}