<?php
namespace Rubeus\Processo\Servico\Regra\Regras;
use Rubeus\ContenerDependencia\Conteiner as Conteiner;

class Cpf extends Regra{
    private $cpf;
    
    public function __construct() {
        parent::__construct('inv_cpf');
        $this->cpf = Conteiner::getInstancia('CPF');
    }
    
    public function validar($valor){
        if(!$valor)return $this->erro = false;
        return $this->cpf->setValidar($valor);
    }
    
}
