<?php
namespace Rubeus\Processo\Servico\Regra\Regras;
use Rubeus\ContenerDependencia\Conteiner as Conteiner;

class Telefone extends Regra{
    private $telefone;
    
    public function __construct() {
        parent::__construct('inv_tel');
        $this->telefone = Conteiner::getInstancia('Telefone');
    }
    
    public function validar($valor){
        if(!$valor || trim($valor) == "" ){
            $this->erro = false;
            return null;
        }
        return $this->telefone->setValidar($valor);
        
    }
    
}
