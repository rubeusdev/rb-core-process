<?php
namespace Rubeus\Processo\Servico\Regra\Regras;
use Rubeus\ContenerDependencia\Conteiner as Conteiner;

class Cep extends Regra{
    private $cep;
    
    public function __construct() {
        parent::__construct('inv_cep');
        $this->cep = Conteiner::getInstancia('CEP');
    }
    
    public function validar($valor){
        if(!$valor)return $this->erro = false;
        return $this->cep->setValidar($valor);
    }
    
}
