<?php
namespace Rubeus\Processo\Servico\Regra\Regras;

class Obrigatorio
{
    private $erro='cp_vz';

    public function validar($valor)
    {
        if (($valor===false || trim(gettype($valor) == 'array' ? json_encode($valor) : $valor)==="" || is_null($valor) || empty($valor)) && $valor !== '0') {
            return false;
        }
        return $valor;
    }

    public function getErro()
    {
        return $this->erro;
    }
}
