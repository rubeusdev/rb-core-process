<?php
namespace Rubeus\Processo\Servico\Regra\Regras;

class Positivo extends Regra{
   
    public function __construct() {
        parent::__construct('vl_pos');
    }
    
    public function validar($valor){
        if(!$valor)return $this->erro = false;
        if(intval($valor) <= 0)return false;
        return $valor;
    }
    
}
