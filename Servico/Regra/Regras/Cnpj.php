<?php
namespace Rubeus\Processo\Servico\Regra\Regras;
use Rubeus\ContenerDependencia\Conteiner as Conteiner;

class Cnpj extends Regra{
    private $cnpj;
    
    public function __construct() {
        parent::__construct('inv_cnpj');
        $this->cnpj = Conteiner::getInstancia('CNPJ');
    }
    
    public function validar($valor){
        if(!$valor)return $this->erro = false;
        return $this->cnpj->setValidar($valor);
    }
    
}
