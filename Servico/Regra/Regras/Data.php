<?php
namespace Rubeus\Processo\Servico\Regra\Regras;
use Rubeus\ContenerDependencia\Conteiner as Conteiner;

class Data extends Regra{
    private $dataHora;
    
    public function __construct() {
        parent::__construct('inv_dt');
        $this->dataHora = Conteiner::getInstancia('DataHora');
    }
    
    public function validar($valor){
        if(!$valor)return $this->erro = false;
        if($this->dataHora->set($valor,'Y-m-d')->dataValida()) return $this->dataHora->get('Y-m-d'); 
        return false;
    }
    
}
