<?php
namespace Rubeus\Processo\Servico\Regra\Regras;
use Rubeus\ContenerDependencia\Conteiner as Conteiner;

class DataNascimento extends Regra{
    private $dataHora;
    
    public function __construct() {
        parent::__construct('inv_nasc_dt');
        $this->dataHora = Conteiner::getInstancia('DataHora',false);
    }
    
    public function validar($valor){
        if(!$valor)return $this->erro = false;
        if($this->dataHora->set($valor,'Y-m-d')->validarData(Conteiner::getInstancia('ValidarNasc'))) return $this->dataHora->get('Y-m-d');
        return false;
    }
    
}
