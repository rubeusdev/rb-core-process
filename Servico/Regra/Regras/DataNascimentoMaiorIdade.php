<?php
namespace Rubeus\Processo\Servico\Regra\Regras;
use Rubeus\ContenerDependencia\Conteiner as Conteiner;

class DataNascimentoMaiorIdade extends Regra{
    private $dataHora;
    
    public function __construct() {
        parent::__construct('inv_nasc_dt');
        $this->dataHora = Conteiner::getInstancia('DataHora',false);
    }
    
    public function validar($valor){
        if(!$valor)return $this->erro = false;
        
        $data = $this->dataHora->set($valor,'Y-m-d');

        $validacao = Conteiner::getInstancia('ValidarNascMaiorIdade')->validar($data);
        
        if($validacao == 1) return $this->dataHora->get('Y-m-d');        
        else if($validacao == 2){
          $this->erro = "inv_nasc_idade";  
        } 


        return false;
    }
    
}
