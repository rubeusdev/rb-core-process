<?php
namespace Rubeus\Processo\Servico\Regra\Regras;

class SobreNome extends Regra{
   
    public function __construct() {
        parent::__construct('er_n_sn');
    }
    
    public function validar($valor){
        if(!$valor){
            return $this->erro = false;
        }
        if(!strpos(trim($valor), ' ')){
            return false;
        }
        return $valor;
    }
    
}
