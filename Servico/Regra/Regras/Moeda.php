<?php
namespace Rubeus\Processo\Servico\Regra\Regras;
use Rubeus\ContenerDependencia\Conteiner as Conteiner;

class Moeda extends Regra{
    private $moeda;
    
    public function __construct() {
        parent::__construct();
        $this->moeda = Conteiner::getInstancia('Moeda');
    }
    
    public function validar($valor){
        if(!$valor)return  $this->erro = false;
        return $this->moeda->set($valor)->float();
    }
    
}
