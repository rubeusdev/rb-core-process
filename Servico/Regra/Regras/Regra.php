<?php
namespace Rubeus\Processo\Servico\Regra\Regras;

abstract class Regra{
    protected $erro;
    private $erroPadrao;
    
    public function __construct($erro=false) {
        $this->erro = $erro;
        $this->erroPadrao = $erro;
    }
    
    abstract public function validar($valor);
    
    public function getErro(){
        $retorno = $this->erro;
        $this->erro = $this->erroPadrao;
        return $retorno;
    }
}
