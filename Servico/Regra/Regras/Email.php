<?php
namespace Rubeus\Processo\Servico\Regra\Regras;
use Rubeus\ContenerDependencia\Conteiner as Conteiner;

class Email extends Regra{
    private $email;
    
    public function __construct() {
        parent::__construct('inv_em');
        $this->email = Conteiner::getInstancia('Email');
    }
    
    public function validar($valor){
        if(!$valor)return $this->erro = false;
        return $this->email->setValidar($valor);
    }
    
}
