<?php
namespace Rubeus\Processo\Servico\Regra\Regras;

class NaoNegativo extends Regra{
   
    public function __construct() {
        parent::__construct('vl_nNeg');
    }
    
    public function validar($valor){
        if(!$valor)return $this->erro = false;
        if(intval($valor) < 0 && !is_array($valor))return false;
        return $valor;
    }
    
}
