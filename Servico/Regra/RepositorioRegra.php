<?php
namespace Rubeus\Processo\Servico\Regra;
use Rubeus\Servicos\Json\Json as Json;
use Rubeus\Processo\Dominio\Exception\ExceptionRegra as ExceptionRegra;

abstract class RepositorioRegra{
    private static $pool;
    private static $teste;
    
    public static function setTeste($teste){
        self::$teste = $teste;
    }
    
    public static function get($entidade){
        if(is_null(self::$pool)) self::$pool = Json::lerArq(__DIR__.'/Entidades.json');
        if(isset(self::$pool->$entidade)){
            $classe = self::$pool->$entidade;
            return new $classe();
        }else if(self::$teste == 1){
            throw new ExceptionRegra('Regra não encontrada!!!', array('regra' => $entidade));
        }else{
            $classe = self::$pool->Opcional;
            return new $classe();
        }
    }
    
}