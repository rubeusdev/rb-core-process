<?php
namespace Rubeus\Processo\Dominio\Evento;

abstract class FabricaEvento{
   
    public static function construir($dados, $codigo) {
        $dados['codigo'] = $codigo;
        return new Evento($dados);
    }
    
}