<?php
namespace Rubeus\Processo\Dominio\Evento;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Processo\Dominio\Exception\ExceptionEvento;

abstract class RepositorioEvento{
    private static $evento;
    
    public static function get($evento) {
        
        if(isset(self::$evento[$evento])) 
            return FabricaEvento::construir(self::$evento[$evento],$evento);
        
        $repositorio = Conteiner::getInstancia('RepositorioEventoInfra');
        $resultado = $repositorio::consultarEvento($evento);
        
        self::$evento[$evento] = $resultado;
        
        if(isset(self::$evento[$evento]))
            return FabricaEvento::construir(self::$evento[$evento], $evento);
        
         throw new ExceptionEvento('Evento foi não encontrado!!!', array('evento' => $evento));
    }
    
}