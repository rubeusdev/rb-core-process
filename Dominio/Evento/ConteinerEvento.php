<?php
namespace Rubeus\Processo\Dominio\Evento;


abstract class ConteinerEvento{
   
    public static function dispararEventos($codigo, $processo=false){
        if(is_array($codigo)){
            foreach($codigo as $codEvento){
                $evento = RepositorioEvento::get($codEvento);
                $evento->execultar($processo);
            }
        }else{
           $evento = RepositorioEvento::get($codigo);
            $evento->execultar($processo); 
        }
    }
    
}