<?php
namespace Rubeus\Processo\Dominio\Evento;
use Rubeus\Processo\Dominio\Entidade\RepositorioEntidade as RepositorioEntidade;

class Evento{
    private $acao;
    private $processo;
    private $codigo;
    
    public function __construct($dados=false) {
       if($dados)$this->popular($dados);
    }
    
    public function popular($dados){
        $this->processo = $dados['processo'];
        $this->acao = $dados['acao'];
        $this->codigo = $dados['codigo'];
    }
    
    public function serialize(){
        return array('acao' => $this->acao, 'processo' => $this->processo, 'codigo' => $this->codigo);
    }
   
    public function execultar($processo=false){
       
        foreach($this->acao as $acao){
            $instancia = RepositorioEntidade::get($acao['entidade']);
            $instancia->{$acao->metodo}($processo->getMensagem());
        }
        
        foreach($this->processo as $processoEscutando){
            $iniciarProcesso = RepositorioProcesso::get($processoEscutando['codigo'], $processoEscutando['etapa']);
            $iniciarProcesso->executar(); 
        }
    }
    
}