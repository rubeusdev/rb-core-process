<?php
namespace Rubeus\Processo\Dominio\Processo;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Processo\Dominio\Exception\ExceptionProcesso;

abstract class RepositorioProcesso{
    private static $processo;
    
    private static function indiceValido($etapa, $processo){
        return $etapa ? $processo.'/'.$etapa : $processo;
    }
    
    public static function get($processo, $etapa=false) {
        $indiceValido = self::indiceValido($etapa, $processo);
        
        if(isset(self::$processo[ $indiceValido])) 
            return FabricaProcesso::construir(self::$processo[$indiceValido], $indiceValido);
        
        
        $repositorio = Conteiner::getInstancia('RepositorioProcessoInfra');
        $resultado = $repositorio::consultarProcesso($processo,$etapa);
        
        self::$processo[$indiceValido] = $resultado;
                
        if(isset(self::$processo[$indiceValido]))
            return FabricaProcesso::construir(self::$processo[$indiceValido], $indiceValido);
        
        throw new ExceptionProcesso('Processo foi não encontrado!!! ==> '.$indiceValido, array('processo' => $indiceValido));
    }
    
}