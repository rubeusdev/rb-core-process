<?php
namespace Rubeus\Processo\Dominio\Processo;
use Rubeus\Bd\Persistencia;

class ApurarResultadoProcesso{
    private $erros;
    private $considerar;
    private $errors;
    private $ultimoCommit;
    private $interromper=false;
    
    public function __construct() {
        $this->erros = array();
        $this->errors = array();
        $this->considerar = array();
        $this->ultimoCommit = array();
    }
    
    public function getInterromper(){
        return $this->interromper;
    }
    
    private function adicionarEtapaConsiderada($resultadoEtapa){ 
        $this->considerar[] = $resultadoEtapa['success'];
        if(!$resultadoEtapa['success']){
            if(isset($resultadoEtapa['dadosExtra']['erro']) && is_array($resultadoEtapa['dadosExtra']['erro']))
                $this->erros = array_merge($this->erros,$resultadoEtapa['dadosExtra']['erro']);
            $this->errors = $resultadoEtapa['msg'];
            $this->interromper = (boolean)true;
            
        }
    }
    
    public function addResultado($mensagem, $etapa){
        //var_dump($etapa, $mensagem->getResultadoEtapa($etapa['codigo']));
//        echo "=========================================================";
        switch ($etapa['commit']){
            case '0':
                break;
            case '5':
                $this->adicionarEtapaConsiderada($mensagem->getResultadoEtapa($etapa['codigo']));
                $this->commit($etapa['codigo']);
                $this->considerar = array();
                break;
            default :
                $this->adicionarEtapaConsiderada($mensagem->getResultadoEtapa($etapa['codigo']));
                break;
        }
    }
    
    private function commit($etapa){
        foreach ($this->considerar as $conciderar){
            if(!$conciderar){
                $this->ultimoCommit[$etapa] = 0;
                Persistencia::roolBack();
                return;
            }
        }
        $this->ultimoCommit[$etapa] = 1;
        Persistencia::commit();
    }
    
    public function resultado($resultadoBase,$etapa){
        if(count($this->ultimoCommit) == 0){
            $this->adicionarEtapaConsiderada($resultadoBase);
            $this->commit($etapa);
        }
        
        if($this->ultimoCommit[$etapa] == 1){
            $this->ultimoCommit = [];
            return $resultadoBase;
        }
        $resultadoBase['dadosExtra']['erro'] = $this->erros;
        $resultadoBase['msg'] = $this->errors;
        $resultadoBase['success'] = false;
        $this->ultimoCommit = [];
        return $resultadoBase;  
    }
}