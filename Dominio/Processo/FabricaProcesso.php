<?php
namespace Rubeus\Processo\Dominio\Processo;

abstract class FabricaProcesso{
   
    public static function construir($dados, $codigo) {
        $dados['codigo'] = $codigo;
        $dados['etapaInicial'] = key($dados['atividades']);
        //var_dump($dados);
        return new Processo($dados);
    }
    
}