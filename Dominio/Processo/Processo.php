<?php
namespace Rubeus\Processo\Dominio\Processo;
use Rubeus\Processo\Dominio\Evento\ConteinerEvento;
use Rubeus\Processo\Dominio\Mensagem\FabricaMensagem;
use Rubeus\Processo\Dominio\Atividade\FabricaAtividade;
use Rubeus\Processo\Dominio\Entidade\RepositorioEntidade;
use Rubeus\Servicos\Entrada\Sessao;

class Processo{
    private $eventoInicial;
    private $eventoFinal;
    private $atividades;
    private $etapaAtual;
    private $mensagem;
    private $indiceEtapa;
    private $ultimaEtapa;
    private $codigo;
    private $etapaRetorno;
    private $apurarResultado;
    private $campos;
    private $etapaRetornoExecutada;
    private $entidadeRetorno;
    private $chamarEntidadeRetorno;
    private $permissao;

    public function __construct($dados) {
        $this->eventoFinal = $dados['eventoFinal'];
        $this->eventoInicial= $dados['eventoInicial'];
        $this->etapaAtual = $dados['etapaInicial'];
        $this->atividades = $dados['atividades'];
        $this->indiceEtapa = 0;
        $this->codigo = $dados['codigo'];
        $this->etapaRetorno = $dados['etapaRetorno'];
        $this->entidadeRetorno = $dados['entidadeRetorno'];
        $this->campos = $dados['campos'];
        $this->permissao = $dados['permissao'];
        if (!defined("UTILIZAR_TRANSACAO")) {
            if ($dados['naoUtilizarTransacao'] == 1) {
                define("UTILIZAR_TRANSACAO", 0);
            } else {
                define("UTILIZAR_TRANSACAO", 1);
            }
        }
        $this->metodo = $dados['metodo'];
        $this->apurarResultado = new ApurarResultadoProcesso();
        $this->etapaRetornoExecutada = false;
        $this->chamarEntidadeRetorno = false;
    }

    public function localizarEtapa($etapa){
        if(isset($this->atividades[$etapa])){
            return FabricaAtividade::construir($this->atividades[$etapa],$etapa);
        }

        if(isset($this->eventoFinal[$etapa])&& !is_null($this->eventoFinal[$etapa]) &&  trim($this->eventoFinal[$etapa]) != '')
            ConteinerEvento::dispararEventos($this->eventoFinal[$etapa],$etapa);
        return false;
    }

    public function setEtapaAtual($etapa){
        $this->ultimaEtapa = $this->etapaAtual;
        $this->etapaAtual = $etapa;
    }

    public function getEtapaAtual(){
        return $this->etapaAtual;
    }

    public function getMensagem(){
        return $this->mensagem;
    }

    public function pecorrerEtapas(){
        $etapaAtual = $this->etapaAtual;

        $etapa = $this->localizarEtapa($etapaAtual);
        if(!$etapa) return;
        if($etapaAtual ===  $this->etapaRetorno)
            $this->etapaRetornoExecutada = true;

        $etapa->executar($this);

        $this->apurarResultado->addResultado($this->mensagem, $this->atividades[$etapaAtual]);
        if(!$this->apurarResultado->getInterromper()){
            $this->pecorrerEtapas();
        }
    }


    public function executar($mensagem=false,$chamarEntidadeRetorno=false){

        if($mensagem){
            $this->mensagem = $mensagem;
        }else{
            $this->mensagem = FabricaMensagem::construir($this->campos, $this->codigo);
        }
        if($this->mensagem->getCampo('EntradaInvalida')->get('valor') == 1){
            $this->mensagem->getCampo('EntradaInvalida')->set('valor',0);
            return $this->mensagem->getResultadoEstruturado($this->mensagem->getCampo('ResultadoValidacao')->get('valor'));
        }

        if($this->permissao && (CLASSE_PERMISSAO!='CLASSE_PERMISSAO' || CAMPO_PERMISSAO!='CAMPO_PERMISSAO')){
            if(CLASSE_PERMISSAO!='CLASSE_PERMISSAO'){
                $class = CLASSE_PERMISSAO;
                $obj = new $class();
                $metodo = $this->metodo;
                if(!$metodo){
                    $metodo = 'validar';
                }
                if(!$obj->$metodo($this->permissao)){
                    return $this->mensagem->getResultadoEstruturado(['success' => false, 'msg' => 'sem_per', 'dadosExtra' => ['semPermissao' => 1]]);
                }
            }elseif(!Sessao::permissaoConta($this->permissao)){
                return $this->mensagem->getResultadoEstruturado(['success' => false, 'msg' => 'sem_per', 'dadosExtra' => ['semPermissao' => 1]]);
            }
        }

        if(isset($this->eventoInicial)&& !is_null($this->eventoInicial) &&
                ((!is_array($this->eventoInicial) && trim($this->eventoInicial) != '' )
                || (is_array($this->eventoInicial) && trim($this->eventoInicial[0]) != '' ))){
            ConteinerEvento::dispararEventos($this->eventoInicial);
        }

        $this->chamarEntidadeRetorno = $chamarEntidadeRetorno;

        $this->pecorrerEtapas();
        return $this->resultado();
    }

    private function resultado(){
        if(trim($this->etapaRetorno) === '' || !$this->etapaRetornoExecutada){
            $etapa = $this->ultimaEtapa;
        }else{
            // $resultado = $this->mensagem->getResultadoEtapa($this->ultimaEtapa);
            if(!$this->apurarResultado->getInterromper()){
                $etapa = $this->etapaRetorno;
            }else{
                $etapa = $this->ultimaEtapa;
            }
        }
        return $this->mensagem->getResultadoEstruturado($this->tratarRetorno($etapa));
    }

    private function tratarRetorno($etapa){
        if($this->entidadeRetorno && $this->chamarEntidadeRetorno){
            $entidade = explode(';', $this->entidadeRetorno);
            for($i=0;$i<count($entidade);$i++){
                $entidadeRetorno = RepositorioEntidade::get(trim($entidade[$i]));
                $retorno = $entidadeRetorno->tratarRetorno($this->mensagem, $etapa);
            }
            return $retorno;
        }
        return  $this->apurarResultado->resultado($this->mensagem->getResultadoEtapa($etapa), $etapa);
    }
}
