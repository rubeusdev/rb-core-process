<?php

namespace Rubeus\Processo\Dominio\Atividade;

use Rubeus\Processo\Dominio\Entidade\RepositorioEntidade;

class Atividade
{
    private $proximo;
    private $entidade;
    private $metodo;
    private $codigo;
    private $commit;
    private static $historico;

    public function __construct($dados = false)
    {
        if ($dados) $this->popular($dados);
    }

    public function popular($dados)
    {
        $this->proximo = $dados['proximo'];
        $this->entidade = $dados['entidade'];
        $this->metodo = $dados['metodo'];
        $this->codigo = $dados['codigo'];
        $this->commit = $dados['commit'];
    }

    public function serialize()
    {
        return array(
            'proximo' => $this->proximo,
            'entidade' => $this->entidade,
            'metodo' => $this->metodo,
            'codigo' => $this->codigo,
            'commit' => $this->commit
        );
    }

    public function executar($processo)
    {
        $temporizador = [
            'codigo' => $this->codigo,
            'inicio' => microtime(true)
        ];
        $instancia = RepositorioEntidade::get($this->entidade);
        $processo->getMensagem()->setEtapaAtual($this->codigo);

        $instancia->{$this->metodo}($processo->getMensagem());
        $temporizador['fim'] = microtime(true);
        $temporizador['decorrido'] = number_format($temporizador['fim'] - $temporizador['inicio'], 3) . 's';
        self::$historico[] = $temporizador;
        $processo->setEtapaAtual($this->proximo);
    }

    public static function getHistorico()
    {
        return self::$historico;
    }
}
