<?php
namespace Rubeus\Processo\Dominio\Atividade;

abstract class FabricaAtividade{
   
    public static function construir($dados, $codigo) {
        $dados['codigo'] = $codigo;
        return new Atividade($dados);
    }
    
}