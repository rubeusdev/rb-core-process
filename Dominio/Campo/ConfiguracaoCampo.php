<?php

namespace Rubeus\Processo\Dominio\Campo;

class ConfiguracaoCampo
{
    private $regra;
    private $chave;
    private $valor;
    private $nome;
    private $foiEnviado;

    public function __construct($dados = false)
    {
        if ($dados) {
            $this->popular($dados);
        }
    }

    public function popular($dados)
    {
        $this->chave = $dados['chave'];
        $this->regra = $dados['regra'];
        $this->nome = $dados['nome'];
        if (is_array($dados['valor']) && count($dados['valor']) == 1 && isset($dados['valor'][0]) && trim(gettype($dados['valor'][0]) == 'array' ? json_encode($dados['valor'][0]) : $dados['valor'][0]) === '') {
            $this->valor = false;
        } else {
            $this->valor = $dados['valor'];
        }
    }

    public function serilaze()
    {
        return array('chave' => $this->chave, 'regra' => $this->regra, 'nome' => $this->nome, 'valor' => $this->valor);
    }

    /**
     * Recupera a propriedade do campo préviamente passado
     *
     * @param string $propriedade Nome da propriedade a ser retornada
     * @param mixed $valorPadrao Valor padrão a ser retornado caso a propriedade não exista
     * @return mixed Valor da propriedade ou o valor padrão
     */
    public function get($propriedade, $valorPadrao = false)
    {

        return ($this->$propriedade || $this->$propriedade === 0 || $this->$propriedade === '0' || is_null($this->$propriedade)) ? $this->$propriedade : $valorPadrao;
    }

    public function set($propriedade, $valor)
    {
        $this->$propriedade = $valor;
        return $this;
    }
}
