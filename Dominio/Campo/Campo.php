<?php
namespace Rubeus\Processo\Dominio\Campo;
use Rubeus\Processo\Servico\Regra\RepositorioRegra;

class Campo{
    private $erro;
    
    public function criar($config) {
        if($config === false)return false;
        
        $this->erro = false;
        $resultCampo = $this->validar($config->get('regra'), $config->get('valor'));
        $config->set('valor',$resultCampo);
        return $resultCampo;
    }

    private function validar($regra, $valor){
        if(!$regra) return $valor;        
        $regras = explode(';',$regra);
        if(is_array($valor)){
            return $this->validarArrayValor($regras, $valor);
        }
        return $this->validarRegra($regras, $valor);
    }
    
    private function validarArrayValor($regras, $valor){
        foreach($valor as $k=>$v){
            $valor[$k] = $this->validarRegra($regras, $v);
        }
        return $valor;
    }
    
    private function validarRegra($regras, $valor){
        foreach($regras as $regra){
            $objRegra = RepositorioRegra::get($regra);
            $resultCampo = $objRegra->validar($valor);
            $erro = $objRegra->getErro();
            if(!$resultCampo &&  $resultCampo !== 0 && $resultCampo !== '0' && $erro){
                $this->erro = $erro;
                return false;
            }
        }
        return $resultCampo;
    }
    
    public function getErro() {
        return $this->erro;
    }

}
