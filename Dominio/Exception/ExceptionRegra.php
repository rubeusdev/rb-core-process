<?php
namespace Rubeus\Processo\Dominio\Exception;

class ExceptionRegra extends \Exception{ 
    private $dados;
    
    public function __construct($message, $dados=false, $code=1, $previous=null) {
        $this->dados = $dados;
        parent::__construct($message, $code, $previous);
    }
    
    public function getMessagem(){
        var_dump($this->dados);
        return parent::getMessage();
    }
}