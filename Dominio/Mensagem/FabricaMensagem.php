<?php
namespace Rubeus\Processo\Dominio\Mensagem;
use Rubeus\Servicos\Entrada\I;
use Rubeus\Servicos\Entrada\Sessao;
//use Rubeus\Controle\DG;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Processo\Dominio\Campo\ConfiguracaoCampo;
use Rubeus\Processo\Dominio\Campo\Campo;

abstract class FabricaMensagem{
    private static $campo;
    private static $erro;

    public static function construir($campos,$processo=false) {
        self::$campo = new Campo();
        $arrayCampoRequisicao = $arrayCampo = array();
        self::$erro = array();
        $mensagem = Conteiner::getInstancia('Mensagem');

        foreach($campos as $campo){
            $valorGetPost = I::post($campo['nome'], I::get($campo['nome']));
            
            $valorCampo = $mensagem->getCampo($campo['chave'],'requisicao');

            $arrayCampo[$campo['chave']] = new ConfiguracaoCampo(array('regra' => $campo['regra'], 'chave' => $campo['chave'], 'nome' => $campo['nome'],'id' => $campo['nome'],
                                                                    'valor' => self::valorCampo ($campo, $valorCampo)));

            $dataJson = json_decode(file_get_contents("php://input"), true);
            $arrayCampo[$campo['chave']]->set(
                'foiEnviado',
                isset($_GET[$campo['nome']]) ||
                isset($_POST[$campo['nome']]) ||
                isset($dataJson[$campo['nome']]) ? 1 : 0
            );

            self::validarCampo($arrayCampo[$campo['chave']]);
                if($campo['requisicao'] == 1){
                    $arrayCampoRequisicao[$campo['chave']] = $arrayCampo[$campo['chave']];
                }
       }

        $arrayCampo['entidade'] = new ConfiguracaoCampo(array('regra' => false, 'chave' =>  'entidade','nome' => false,
                                                        'valor' =>Sessao::get(Sessao::get('usuarioSessao').','.Sessao::get('aba').',entidade')));
        if(!empty(self::$erro)){
            $mensagem->setCampo('EntradaInvalida',1,'requisicao');
            $mensagem->setCampo('ResultadoValidacao',array('success' => false, 'msg' => false,'dadosExtra' => array('erro' => self::$erro)),'requisicao');
        }
        if(count($arrayCampoRequisicao) > 0){
            $mensagem->iniciar($arrayCampoRequisicao,'requisicao', false, false, $processo);
        }
        return $mensagem->iniciar($arrayCampo,'processo', false, false, $processo);
    }

    private static function validarCampo(&$dadosCampo){
        $valor = self::$campo->criar($dadosCampo);

        if(self::$campo->getErro()){
            self::$erro[] = ['campo' => $dadosCampo->get('nome'), 'erro' => self::$campo->getErro()];
        }else{
            $dadosCampo->set('valor', $valor);
        }
    }

    private static  function dividirCampo($campo){
        $campos = explode('-',$campo);
        $campoDividido = array();
        foreach($campos as $cp){
            $valores = explode('::',$cp);
            $campoDividido[] = array('chave' => $valores[0],'valor' => $valores[1] !== '' ? $valores[1]:false);
        }
        return $campoDividido;
    }

    private static function valorCampo($campo, $valorCampo){
        $valor = $valorCampo->get('valor', I::post($campo['nome'], I::get($campo['nome'], I::files($campo['nome'],
                Sessao::get($campo['nome'],
                    Sessao::get(Sessao::get('usuarioSessao').','.Sessao::get('aba').','.$campo['nome'],$campo['valorPadrao']))))));
        if($campo['json'] == 1 && I::get('ambTeste') == 1){
            return \Rubeus\Servicos\Json\Json::lerJsonComoArray($valor);
        }
        return $valor;
    }
}
