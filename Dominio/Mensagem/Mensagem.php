<?php

namespace Rubeus\Processo\Dominio\Mensagem;

use ArrayAccess;
use Rubeus\Servicos\Entrada\Sessao;
use Rubeus\Processo\Dominio\Campo\ConfiguracaoCampo;

class Mensagem implements ArrayAccess
{
    private $resultadoEtapa;
    private $campo = array();
    private $etapas;
    private $aba;
    private $processo;
    private $usuario;
    private $codigoEtapaAtual;

    /**
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->setCampo($offset, $value);
    }

    /**
     * @return bool
     */
    public function offsetExists($offset)
    {
        return $this->getCampo($offset)->get('valor', null) !== null;
    }

    /**
     * @return void
     */
    public function offsetUnset($offset)
    {
        // unset($this->container[$offset]);
    }

    /**
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->getCampo($offset)->get('valor');
    }

    /**
     * @return void
     */
    function __set($set, $val)
    {
        $this->setCampo($set, $val);
    }

    /**
     * @return mixed
     */
    function __get($get)
    {
        return $this->getCampo($get)->get('valor');
    }

    public function getProcesso()
    {
        return $this->processo;
    }

    public function iniciar($campo = array(), $tipo = 'processo', $usuario = false, $aba = false, $processo = '')
    {
        $this->resultadoEtapa = array();
        $this->aba = $aba;
        $this->usuario = $usuario ? $usuario : Sessao::get('usuarioSessao');
        $this->aba = $aba ? $aba : Sessao::get('aba');
        $this->processo = $processo;
        $this->campo[$tipo] = $campo;
        return $this;
    }

    /**
     * @return ConfiguracaoCampo
     */
    public function getCampo($chave, $tipo = 'processo')
    {
        if ((!isset($this->campo['processo'][$chave]) ||  is_null(($this->campo['processo'][$chave])))
            && (!isset($this->campo['requisicao'][$chave]) ||  is_null(($this->campo['requisicao'][$chave])))
            && (!isset($this->campo['sessao'][$chave]) ||  is_null(($this->campo['sessao'][$chave])))
        ) {
            $this->campo[$tipo][$chave]
                = new ConfiguracaoCampo(array('regra' => false, 'chave' => $chave, 'nome' => false, 'valor' => false));
        }
        if (isset($this->campo[$tipo][$chave])) {
            return $this->campo[$tipo][$chave];
        }
        if (isset($this->campo['processo'][$chave]) &&  !is_null(($this->campo['processo'][$chave]))) {
            return $this->campo['processo'][$chave];
        }
        if (isset($this->campo['requisicao'][$chave]) &&  !is_null(($this->campo['requisicao'][$chave]))) {
            return $this->campo['requisicao'][$chave];
        }
        if (isset($this->campo['sessao'][$chave]) &&  !is_null(($this->campo['sessao'][$chave]))) {
            return $this->campo['sessao'][$chave];
        }
    }

    public function getCampoSessao($chave, $valorPadrao = false, $aba = false)
    {
        if (Sessao::get($this->usuario . ',' . $this->aba . ',' . $chave) && $aba) {
            return Sessao::get($this->usuario . ',' . $this->aba . ',' . $chave, $valorPadrao);
        }
        return Sessao::get($chave, $valorPadrao);
    }

    public function  getResultadoEtapa($etapa)
    {
        return $this->resultadoEtapa[$etapa];
    }

    public function  getResultadoEstruturado($resultado)
    {
        return R::estRet($resultado['success'], $resultado['msg'], $resultado['dadosExtra']);
    }

    public function setCampo($chave, $valor, $tipo = 'processo')
    {
        if (isset($this->campo[$tipo][$chave])) {
            $this->campo[$tipo][$chave]->set('valor', $valor);
        } else {
            $this->campo[$tipo][$chave] = new ConfiguracaoCampo(array('regra' => false, 'chave' => $chave, 'nome' => false, 'valor' => $valor));
        }
        return $this;
    }

    public function addCampo($chave, $valor, $tipo = 'processo')
    {
        if (isset($this->campo[$tipo][$chave])) {
            $campo = $this->campo[$tipo][$chave]->get('valor');
            $campo[] = $valor;
            $this->campo[$tipo][$chave]->set('valor', $campo);
        } else {
            $this->campo[$tipo][$chave] = new ConfiguracaoCampo(array('regra' => false, 'chave' => $chave, 'nome' => false, 'valor' => array($valor)));
        }
    }

    public function setCampoSessao($chave, $valor, $processo = false, $tipo = 'processo')
    {
        if (isset($this->campo[$tipo][$chave])) {
            $this->campo[$tipo][$chave]->set('valor', $valor);
        } else {
            $this->campo[$tipo][$chave] = new ConfiguracaoCampo(array('regra' => false, 'chave' => $chave, 'nome' => false, 'valor' => $valor));
        }
        if ($processo) {

            Sessao::set($this->usuario . ',' . $this->aba . ',' . $chave, $valor);
        } else {
            Sessao::set($chave, $valor);
        }
        return $this;
    }

    public function addCampoSessao($chave, $valor, $processo = true, $tipo = 'processo')
    {
        $campo = $this->getCampoSessao($chave, $processo);
        if (!$campo) {
            $campo = array();
        }
        $campo[] = $valor;
        $this->setCampoSessao($chave, $campo, $processo, $tipo);
    }

    public function setResultadoEtapa($success, $codMensagem = false, $dadosExtra = false)
    {
        $this->resultadoEtapa[$this->codigoEtapaAtual] = array('success' => $success, 'msg' => $codMensagem, 'dadosExtra' => $dadosExtra);
        $this->etapas[] = $this->codigoEtapaAtual;
    }

    public function getEtapasConcluida()
    {
        return $this->etapas;
    }

    public function setEtapaAtual($etapa)
    {
        $this->codigoEtapaAtual = $etapa;
    }

    public function getEtapaAtual()
    {
        return $this->codigoEtapaAtual;
    }

    public function acessarAbaAtual()
    {
        $this->aba = Sessao::get('aba');
        return $this;
    }

    public function acessarAbaPai()
    {
        $this->aba = Sessao::get('abaPai');
        return $this;
    }

    public function limparSessao()
    {
        Sessao::destroiSessao();
    }

    public function getDump()
    {
        var_dump($this->campo);
    }

    public function getCampos()
    {
        $campos = [];
        foreach ($this->campo as $key => $value) {
            if (!isset($campos[$key])) $campos[$key] = [];
            foreach ($value as $k => $v) {
                if (!isset($campos[$key][$k])) $campos[$key][$k] = [];
                $campos[$key][$k] = ['regra' => $v->get('regra'), 'chave' => $v->get('chave'), 'nome' => $v->get('nome'), 'valor' => $v->get('valor')];
            }
        }
        return $campos;
    }

    public function setCampos($campo)
    {
        foreach ($campo as $key => $value) {
            if (!isset($this->campo[$key])) $this->campo[$key] = [];
            foreach ($value as $k => $v) {
                if (!isset($this->campo[$key][$k])) $this->campo[$key][$k] = [];
                $this->campo[$key][$k] = new ConfiguracaoCampo(['regra' => $v['regra'], 'chave' => $v['chave'], 'nome' => $v['nome'], 'valor' => $v['valor']]);
            }
        }
    }
}
