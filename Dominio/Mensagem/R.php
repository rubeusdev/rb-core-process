<?php
namespace Rubeus\Processo\Dominio\Mensagem;
use Rubeus\Servicos\Json\Json;

abstract  class R{    
    private static $origem = __DIR__;
    private static $idiomaDir;


    public static function estRet($success, $id=false, $dados=false) {      
        $dadosRetorno = !is_null($success)?array('success' => $success):array();
        if($id){
            $nome = $success == true ? 'msg' : (is_null($success) ? 'titulo' : 'errors');
            $dadosRetorno[$nome] = self::estMensagem($id);
        }
        if($dados){
            foreach ($dados as $key=>$value){
                $dadosRetorno[$key] = $value;
            }
        }
        if(isset($dadosRetorno['erro'])){
            for($i=0; $i < count($dadosRetorno['erro']);$i++){
                $dadosRetorno['erro'][$i]['erro'] =  self::estMensagem($dadosRetorno['erro'][$i]['erro']);
            }
        }
        return $dadosRetorno;
    }
    
    private static function estMensagem($id){
        if(!is_array($id)){
            $dadosRetorno = R::getMensagem($id);
        }else{
            $array = array();
            foreach($id as $i){
                $array[] = self::getMensagem($i);
            }
            $dadosRetorno = $array;
        }
        return $dadosRetorno;
    }
    
    public static function getMensagem($id){       
        $idiomaConfig = self::$idiomaDir;

        if(is_null($id) || $id==false){
            return "";
        }

        if(isset($idiomaConfig)) {
            $mensagem = Json::lerArq(self::$origem."/mensagens/".$idiomaConfig."/mensagem.json"); 
            
            if(!isset($mensagem->$id)){
                $mensagem = Json::lerArq(DIR_BASE."/src/mensagens/".$idiomaConfig."/mensagem.json");
                if(!isset($mensagem->$id)){
                    return "";  
                }
                return $mensagem->$id;
            }
            return $mensagem->$id;
        } else {
            $mensagem = Json::lerArq(self::$origem."/mensagens/pt-br/mensagem.json");
            if(!isset($mensagem->$id)){
                $mensagem = Json::lerArq(DIR_BASE."/src/mensagem.json");
                if(!isset($mensagem->$id)){
                    return "";  
                }
                return $mensagem->$id;
            }
            return $mensagem->$id;
        }      
    }

    public static function setDir($idiomaDir){
        self::$idiomaDir = $idiomaDir;
    }
}