<?php
namespace Rubeus\Processo\Dominio\Entidade;
use Rubeus\Servicos\Json\Json;
use Rubeus\ContenerDependencia\Conteiner;

abstract class RepositorioEntidade{
    static private $pool;
    
    public static function get($entidade){
        return Conteiner::getInstancia($entidade);
    }
    
}
