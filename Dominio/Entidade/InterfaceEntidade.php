<?php
namespace Rubeus\Processo\Dominio;

interface InterfaceEntidade{
    
    public static function serialize();
    
    public static function popular($dados);
    
}